<?php

namespace common\bootstrap;

use Yii;
use yii\base\BootstrapInterface;
use frontend\readModels\TagReadRepository;
use frontend\readModels\MaterialReadRepository;
use frontend\useCases\material\MaterialService;
use frontend\useCases\tag\TagService;
use frontend\useCases\category\CategoryService;
use frontend\repositories\MaterialRepository;
use frontend\repositories\TagRepository;
use frontend\repositories\LinkRepository;
use frontend\repositories\CategoryRepository;
use frontend\repositories\TagToMaterialRepository;

/**
 * Description of SetUp
 *
 * @author kjyra
 */
class SetUp implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;
        
        $container->setSingleton(TagReadRepository::class, function() use($app) {
            return new TagReadRepository();
        });
        
        $container->setSingleton(MaterialReadRepository::class, function() use($app) {
            return new MaterialReadRepository();
        });
        
        $container->setSingleton(TagRepository::class, function() use($app) {
            return new TagRepository();
        });
        
        $container->set(MaterialService::class, function ($container, $params, $config) {
            $materialService = new MaterialService(Yii::$app->db, new MaterialRepository(), new TagRepository(), new LinkRepository(), new TagToMaterialRepository());
            return $materialService;
        });
        
        $container->set(TagService::class, function ($container, $params, $config) {
            $tagService = new TagService(Yii::$app->db, new TagRepository(), new TagToMaterialRepository());
            return $tagService;
        });
        
        $container->set(CategoryService::class, function ($container, $params, $config) {
            $categoryService = new CategoryService(new CategoryRepository());
            return $categoryService;
        });
    }
}
