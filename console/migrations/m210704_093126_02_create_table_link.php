<?php

use yii\db\Migration;

class m210704_093126_02_create_table_link extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%link}}',
            [
                'id' => $this->primaryKey(),
                'material_id' => $this->integer()->notNull(),
                'name' => $this->string(),
                'url' => $this->string()->notNull(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%link}}');
    }
}
