<?php

use yii\db\Migration;

class m210704_093126_03_create_table_material extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%material}}',
            [
                'id' => $this->primaryKey(),
                'type_id' => $this->integer()->notNull(),
                'category_id' => $this->integer()->notNull(),
                'authors' => $this->string(),
                'description' => $this->text(),
                'name' => $this->string()->notNull(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%material}}');
    }
}
