<?php

use yii\db\Migration;

class m210704_093127_05_create_table_tagtomaterial extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%tagtomaterial}}',
            [
                'id' => $this->primaryKey(),
                'material_id' => $this->integer(),
                'tag_id' => $this->integer(),
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%tagtomaterial}}');
    }
}
