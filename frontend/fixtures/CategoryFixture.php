<?php

namespace frontend\fixtures;

use yii\test\ActiveFixture;

/**
 * Description of MaterialFuxture
 *
 * @author grigo
 */
class CategoryFixture extends ActiveFixture
{
    public $modelClass = 'frontend\models\Category';
    public $dataFile = 'frontend/fixtures/data/category.php';
}
