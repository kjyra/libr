<?php

namespace frontend\fixtures;

use yii\test\ActiveFixture;

/**
 * Description of MaterialFuxture
 *
 * @author grigo
 */
class TagFixture extends ActiveFixture
{
    public $modelClass = 'frontend\models\Tag';
    public $dataFile = 'frontend/fixtures/data/tag.php';
}
