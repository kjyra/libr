<?php

return [
    ['id' => 1, 'type_id' => 1, 'category_id' => 1, 'authors' => 'Максим Дорофеев', 'description' => 'Почитать стоит для чиного роста', 'name' => 'Путь Джедая'],
    ['id' => 2, 'type_id' => 2, 'category_id' => 2, 'authors' => 'Александр Макаров', 'description' => 'Документация к фреймворку Yii2', 'name' => 'Полное руководство по Yii 2.0'],
    ['id' => 3, 'type_id' => 3, 'category_id' => 1, 'authors' => 'author1', 'description' => 'description of material 3', 'name' => 'material 3'],
    ['id' => 4, 'type_id' => 1, 'category_id' => 2, 'authors' => 'author2', 'description' => 'description of material 4', 'name' => 'material 4'],
    ['id' => 5, 'type_id' => 3, 'category_id' => 3, 'authors' => 'author3', 'description' => 'description of material 5', 'name' => 'material 5'],
    ['id' => 6, 'type_id' => 4, 'category_id' => 4, 'authors' => 'author4', 'description' => 'description of material 6', 'name' => 'material 6'],
    
];