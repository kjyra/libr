<?php

namespace frontend\fixtures;

use yii\test\ActiveFixture;

/**
 * Description of MaterialFuxture
 *
 * @author grigo
 */
class LinkFixture extends ActiveFixture
{
    public $modelClass = 'frontend\models\Link';
    public $dataFile = 'frontend/fixtures/data/link.php';
    
    public $depends = ['frontend\fixtures\MaterialFixture'];
}
