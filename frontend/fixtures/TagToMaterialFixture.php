<?php

namespace frontend\fixtures;

use yii\test\ActiveFixture;

/**
 * Description of MaterialFuxture
 *
 * @author grigo
 */
class TagToMaterialFixture extends ActiveFixture
{
    public $modelClass = 'frontend\models\TagToMaterial';
    public $dataFile = 'frontend/fixtures/data/tagToMaterial.php';
    
    public $depends = ['frontend\fixtures\TagFixture', 'frontend\fixtures\MaterialFixture'];
}
