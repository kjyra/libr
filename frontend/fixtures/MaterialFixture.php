<?php

namespace frontend\fixtures;

use yii\test\ActiveFixture;

/**
 * Description of MaterialFuxture
 *
 * @author grigo
 */
class MaterialFixture extends ActiveFixture
{
    public $modelClass = 'frontend\models\Material';
    public $dataFile = 'frontend/fixtures/data/material.php';
    
    public $depends = ['frontend\fixtures\TypeFixture', 'frontend\fixtures\CategoryFixture'];
}
