<?php

namespace frontend\readModels;

use frontend\models\Category;

/**
 * Description of TagReadRepository
 *
 * @author kjyra
 */
class CategoryReadRepository
{
    public function all(): array
    {
        return Category::find()->All();
    }
    
    public function get(int $id): Category
    {
        $category = Category::findOne($id);
        if(empty($category)) {
            throw new \RuntimeException('Category not found.');
        }
        return $category;
    }
}
