<?php

namespace frontend\readModels;

use frontend\models\Material;
use frontend\models\TagToMaterial;
use frontend\models\Tag;

/**
 * Description of TagReadRepository
 *
 * @author kjyra
 */
class TagReadRepository
{
    public function all(): array
    {
        return Tag::find()->all();
    }
    
    public function get(int $id): Tag
    {
        $tag = Tag::findOne($id);
        if(empty($tag)) {
            throw new \RuntimeException('Tag not found.');
        }
        return $tag;
    }
    
    public function findNotInMaterial(Material $material): array
    {
        $tagsIdsNotInMaterial = TagToMaterial::find()->where(['material_id' => $material->id])->select('tag_id')->asArray()->all();
        $ids = [];
        foreach($tagsIdsNotInMaterial as $id) {
            $ids[] = $id['tag_id'];
        }
        return Tag::find()->where(['not in', 'id', $ids])->all();
    }
}
