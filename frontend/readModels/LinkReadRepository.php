<?php

namespace frontend\readModels;

use frontend\models\Link;

/**
 * Description of TagReadRepository
 *
 * @author kjyra
 */
class LinkReadRepository
{
    public function find(int $id): ?Link
    {
        return Link::findOne($id);
    }
}
