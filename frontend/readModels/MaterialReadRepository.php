<?php

namespace frontend\readModels;

use frontend\models\Material;
use frontend\models\Category;
use frontend\models\Tag;

/**
 * Description of MaterialReadModel
 *
 * @author kjyra
 */
class MaterialReadRepository
{
    public function all(string $keyword = null): ?array
    {
        if($keyword) {
            return $this->search($keyword);
        }
        return Material::find()->all();
    }
    
    public function get(int $id): Material
    {
        $material = Material::findOne($id);
        if(empty($material)) {
            throw new \RuntimeException('Material not found.');
        }
        return $material;
    }
    
    private function search(string $keyword): ?array
    {
        $result = [];
        if($materials = Material::find()->where(['like', 'name', $keyword . '%', false])->orWhere(['like', 'authors', $keyword . '%', false])->all()) {
            foreach($materials as $material) {
                $result[] = $material;                
            }
        }
        if($tags = Tag::find()->where(['like', 'name', $keyword . '%', false])->all()) {
            foreach($tags as $tag) {
                if(!empty($tag->getMaterials())) {
                    foreach($tag->getMaterials() as $material) {
                        if(!in_array($material, $result)) {
                            $result[] = $material;
                        }                        
                    }
                }
            }
        }
        if($categories = Category::find()->where(['like', 'name', $keyword . '%', false])->all()) {
            foreach($categories as $category) {
                if(!empty($category->getMaterials())) {
                    foreach($category->getMaterials() as $material) {
                        if(!in_array($material, $result)) {
                            $result[] = $material;
                        }                        
                    }
                }               
            }
        }
        return $result;
    }
}
