<?php

namespace frontend\useCases\material;

use RuntimeException;
use frontend\models\Link;
use frontend\models\forms\MaterialForm;
use frontend\models\forms\LinkForm;
use frontend\models\forms\TagForm;
use frontend\models\TagToMaterial;
use frontend\models\Material;
use frontend\repositories\MaterialRepository;
use frontend\repositories\TagRepository;
use frontend\repositories\LinkRepository;
use frontend\repositories\TagToMaterialRepository;
use frontend\models\Tag;
use yii\db\Connection;

/**
 * Description of MaterialService
 *
 * @author kjyra
 */
class MaterialService
{
    private $connection;
    private $tags;
    private $materials;
    private $links;
    private $tagsToMaterial;
    
    public function __construct(
        Connection $connection,
        MaterialRepository $materials,
        TagRepository $tags,
        LinkRepository $links,
        TagToMaterialRepository $tagsToMaterial
    ) {
        $this->connection = $connection;
        $this->materials = $materials;
        $this->tags = $tags;
        $this->links = $links;
        $this->tagsToMaterial = $tagsToMaterial;
    }
    
    public function handleCreate(MaterialForm $form)
    {
        $material = Material::create($form->type, $form->category, $form->name, $form->authors, $form->desc);
        
        return $this->materials->save($material);
    }
    
    public function handleUpdate(MaterialForm $form, int $id)
    {
        $material = $this->materials->get($id);
        $material->updateSelf($form->type, $form->category, $form->name, $form->authors, $form->desc);
        
        return $this->materials->save($material);
    }
    
    public function handleAddLink(LinkForm $form, Material $material)
    {
        $link = Link::create($form->name, $form->url, $material->id);
        
        return $this->links->save($link);
    }
    
    public function handleUpdateLink(LinkForm $form, int $linkId)
    {
        $link = $this->links->get($linkId);
        $link->updateSelf($form->name, $form->url);
        
        return $this->links->save($link);
    }
    
    public function addTag(TagForm $tagForm, Material $material)
    {
        $tag = $this->tags->findByName($tagForm->name);
        $tagToMaterial = TagToMaterial::create($material->id, $tag->id);
        
        return $this->tagsToMaterial->save($tagToMaterial);
    }
    
    public function deleteMaterial(Material $material)
    {
        $tagsToMaterial = $this->tagsToMaterial->findByMaterial($material);
        return $this->deleteMaterialAndRelations($material, $tagsToMaterial);
    }
    
    public function deleteTag(Tag $tag, Material $material)
    {        
        $tagToMaterial = $this->tagsToMaterial->findManyToManyRelations($tag, $material);
        return $this->tagsToMaterial->delete($tagToMaterial);
    }
    
    public function deleteLink(Link $link)
    {        
        return $this->links->delete($link);
    }

    private function deleteMaterialAndRelations(Material $material, array $tagsToMaterial = null)
    {
        $transaction = $this->connection->beginTransaction();
        if($tagsToMaterial) {
            foreach($tagsToMaterial as $tagToMaterial) {
                if(!$tagToMaterial->delete()) {
                    $transaction->rollBack();
                    throw new RuntimeException('Deleting error.');
                }
            }
        }
        
        if(!$material->delete()) {
            $transaction->rollBack();
            throw new RuntimeException('Deleting error.');
        }
        $transaction->commit();
        return true;
    }    
}
