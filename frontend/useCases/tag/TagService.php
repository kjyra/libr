<?php

namespace frontend\useCases\tag;

use RuntimeException;
use frontend\models\Tag;
use frontend\models\forms\TagForm;
use frontend\repositories\TagRepository;
use frontend\repositories\TagToMaterialRepository;
use yii\db\Connection;

/**
 * Description of MaterialService
 *
 * @author kjyra
 */
class TagService
{
    private $connection;
    private $tags;
    private $tagsToMaterial;

    public function __construct(Connection $connection, TagRepository $tags, TagToMaterialRepository $tagsToMaterial)
    {
        $this->connection = $connection;
        $this->tags = $tags;
        $this->tagsToMaterial = $tagsToMaterial;
    }

    public function handleCreate(TagForm $tagForm)
    {
        $tag = Tag::create($tagForm->name);

        return $this->tags->save($tag);
    }

    public function handleUpdate(TagForm $form, int $id)
    {
        $tag = $this->tags->get($id);
        $tag->updateSelf($form->name);

        return $this->tags->save($tag);
    }

    public function handleDelete(Tag $tag)
    {
        $tagsToMaterial = $this->tagsToMaterial->findByTag($tag);
        $this->deleteTagAndRelations($tag, $tagsToMaterial);
    }

    private function deleteTagAndRelations(Tag $tag, array $tagsToMaterial = null)
    {
        $transaction = $this->connection->beginTransaction();
        if ($tagsToMaterial) {
            foreach ($tagsToMaterial as $tagToMaterial) {
                if (!$tagToMaterial->delete()) {
                    $transaction->rollBack();
                    throw new RuntimeException('Deleting error.');
                }
            }
        }

        if (!$tag->delete()) {
            $transaction->rollBack();
            throw new RuntimeException('Deleting error.');
        }
        $transaction->commit();
        return true;
    }
}
