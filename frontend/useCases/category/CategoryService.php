<?php

namespace frontend\useCases\category;

use frontend\models\Category;
use frontend\models\forms\CategoryForm;
use frontend\repositories\CategoryRepository;

/**
 * Description of MaterialService
 *
 * @author kjyra
 */
class CategoryService
{
    private $categories;
    
    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }
    
    public function handleCreate(CategoryForm $categoryForm)
    {
        $category = Category::create($categoryForm->name);
        
        return $this->categories->save($category);
    }
    
    public function handleUpdate(CategoryForm $form, int $id)
    {
        $category = $this->categories->get($id);
        $category->updateSelf($form->name);
        
        return $this->categories->save($category);
    }
    
    public function handleDelete(Category $category)
    {
        return $this->categories->delete($category);
    }    
}
