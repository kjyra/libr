$(document).ready(function() {
    $('.modal-link').on('click', function () {
        $.get("/material/link-modal", {
            'id': $(this).attr("data-id")
        })
                .done(function(data) {
                        $('#exampleModalToggle .modal-content').html(data);
                            $('.form-floating').each(function () {
                                if($(this).find('input')) {
                                    $el = $(this);
                                    $el.find('input').insertBefore( $el.find('label') );    
                                }
                            });
                        $('#exampleModalToggle').modal();
                })
                .fail(function() {
                    alert('ошибка');
                })
    });
});

$(document).ready(function() {
    $('.modal-link-update').on('click', function () {
        $.get("/material/link-modal-update", {
            'id': $(this).attr("data-id")
        })
                .done(function(data) {
                        $('#exampleModalToggle .modal-content').html(data);
                            $('.form-floating').each(function () {
                                if($(this).find('input')) {
                                    $el = $(this);
                                    $el.find('input').insertBefore( $el.find('label') );    
                                }
                            });
                        $('#exampleModalToggle').modal();
                })
                .fail(function() {
                    alert('ошибка');
                })
    });
});

