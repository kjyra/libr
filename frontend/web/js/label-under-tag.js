$(document).ready(function () {
    $('.form-floating').each(function () {
        if($(this).find('select')) {
            $el = $(this);
            $el.find('select').insertBefore( $el.find('label') );    
        }
        if($(this).find('input')) {
            $el = $(this);
            $el.find('input').insertBefore( $el.find('label') );    
        }
        if($(this).find('textarea')) {
            $el = $(this);
            $el.find('textarea').insertBefore( $el.find('label') );    
        }
    });
});