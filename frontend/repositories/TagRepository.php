<?php

namespace frontend\repositories;

use frontend\models\Tag;

/**
 * Description of TagRepository
 *
 * @author grigo
 */
class TagRepository
{
    public function get(int $id) : Tag
    {
        if($tag = Tag::findOne($id)) {
            return $tag;
        }
        throw new \RuntimeException('Tag not found.');
    }
    
    public function findByName(string $name): ?Tag
    {
        return Tag::find()->where(['name' => $name])->one();
    }
    
    public function save(Tag $tag): bool
    {
        if(!$tag->save()) {
            throw new \RuntimeException('Saving error.');
        }
        return true;
    }
}
