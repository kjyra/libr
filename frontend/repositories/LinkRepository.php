<?php

namespace frontend\repositories;

use frontend\models\Link;

/**
 * Description of TagRepository
 *
 * @author grigo
 */
class LinkRepository
{
    public function get(int $id): Link
    {
        $link = Link::findOne($id);
        if(empty($link)) {
            throw new \RuntimeException('Link not found.');
        }
        return $link;
    }
    
    public function save(Link $link): bool
    {
        if(!$link->save()) {
            throw new \RuntimeException('Saving error.');
        }
        return true;
    }
    
    public function delete(Link $link): bool
    {
        if(!$link->delete()) {
            throw new \RuntimeException('Deleting error.');
        }
        return true;
    }
}
