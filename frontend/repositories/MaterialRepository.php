<?php

namespace frontend\repositories;

use frontend\models\Material;

/**
 * Description of TagRepository
 *
 * @author grigo
 */
class MaterialRepository
{
    public function get(int $id): Material
    {
        $material = Material::findOne($id);
        if(empty($material)) {
            throw new \RuntimeException('Material not found.');
        }
        return $material;
    }
    
    public function save(Material $material): bool
    {
        if(!$material->save()) {
            throw new \RuntimeException('Saving error.');
        }
        return true;
    }
}
