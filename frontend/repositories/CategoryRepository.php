<?php

namespace frontend\repositories;

use frontend\models\Category;

/**
 * Description of TagRepository
 *
 * @author grigo
 */
class CategoryRepository
{
    public function get(int $id) : Category
    {
        if($category = Category::findOne($id)) {
            return $category;
        }
        throw new \RuntimeException('Category not found.');
    }
    
    public function save(Category $category): bool
    {
        if(!$category->save()) {
            throw new \RuntimeException('Saving error.');
        }
        return true;
    }
    
    public function delete(Category $category): bool
    {
        if(!$category->delete()) {
            throw new \RuntimeException('Saving error.');
        }
        return true;
    }
}
