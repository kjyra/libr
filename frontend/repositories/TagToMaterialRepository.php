<?php

namespace frontend\repositories;

use frontend\models\TagToMaterial;
use frontend\models\Material;
use frontend\models\Tag;

/**
 * Description of TagToMaterialRepository
 *
 * @author grigo
 */
class TagToMaterialRepository
{
    public function findManyToManyRelations(Tag $tag, Material $material)
    {
        return TagToMaterial::find()->where(['tag_id' => $tag->id])->andWhere(['material_id' => $material->id])->one();
    }
    
    public function findByTag(Tag $tag): ?array
    {
        return TagToMaterial::find()->where(['tag_id' => $tag->id])->all();
    }
    
    public function findByMaterial(Material $material): ?array
    {
        return TagToMaterial::find()->where(['material_id' => $material->id])->all();
    }
    
    public function save(TagToMaterial $tagToMaterial): bool
    {
        if(!$tagToMaterial->save()) {
            throw new \RuntimeException('Saving error.');
        }
        return true;
    }
    
    public function delete(TagToMaterial $tagToMaterial): bool
    {
        if(!$tagToMaterial->delete()) {
            throw new \RuntimeException('Deleting error.');
        }
        return true;
    }
}
