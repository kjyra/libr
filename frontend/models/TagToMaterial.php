<?php

namespace frontend\models;


/**
 * This is the model class for table "tagToMaterial".
 *
 * @property int $id
 * @property int|null $material_id
 * @property int|null $tag_id
 */
class TagToMaterial extends \yii\db\ActiveRecord
{
    public static function create(int $material_id, int $tag_id): self
    {
        $tagToMaterial = new static();
        $tagToMaterial->material_id = $material_id;
        $tagToMaterial->tag_id = $tag_id;
        return $tagToMaterial;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tagToMaterial';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'tag_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'tag_id' => 'Tag ID',
        ];
    }
}
