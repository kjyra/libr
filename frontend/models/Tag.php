<?php

namespace frontend\models;


/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property string $name

 * @property Material[] $materials
 */
class Tag extends \yii\db\ActiveRecord
{
    public static function create(string $name)
    {
        $tag = new static();
        $tag->name = $name;
        return $tag;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    
    public function updateSelf(string $name)
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * @return Material[]
     */
    public function getMaterials()
    {
        return $this->hasMany(Material::class, ['id' => 'material_id'])->viaTable('tagToMaterial', ['tag_id' => 'id'])->all();
    }
}
