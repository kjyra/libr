<?php

namespace frontend\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * Description of MaterialTypes
 *
 * @author grigo
 */
class MaterialTypes extends BaseEnum
{
    const BOOK = 0;
    const VIDEO = 1;
    const POST = 2;
    const SITE = 3;
    
    public static $messageCategory = 'app';
    
    /**
     * @var array
     */
    public static $list = [
        self::BOOK => 'Книга',
        self::VIDEO => 'Видео',
        self::POST => 'Статья',
        self::SITE => 'Сайт',
    ];
}
