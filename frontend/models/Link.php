<?php

namespace frontend\models;

/**
 * This is the model class for table "link".
 *
 * @property int $id
 * @property int $material_id
 * @property string|null $name
 * @property string $url
 */
class Link extends \yii\db\ActiveRecord
{
    public static function create(string $url, int $material_id, string $name = null)
    {
        $link = new static();
        $link->name = $name ?: null;
        $link->material_id = $material_id;
        $link->url = $url;
        return $link;
    }
    
    public function updateSelf(string $url, string $name = null)
    {
        $this->name = $name ?: $this->name;
        $this->url = $url;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'link';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'url'], 'required'],
            [['material_id'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'name' => 'Name',
            'url' => 'Url',
        ];
    }
}
