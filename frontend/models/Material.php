<?php

namespace frontend\models;

use frontend\models\enums\MaterialTypes;

/**
 * This is the model class for table "material".
 *
 * @property int $id
 * @property int $type_id
 * @property int $category_id
 * @property string $name
 * @property string|null $authors
 * @property string|null $description

 * @property string $type
 * @property Category $category
 * @property Tag[] $tags
 * @property Link[] $links
 */
class Material extends \yii\db\ActiveRecord
{
    public static function create(
        int $type_id,
        int $category_id,
        string $name,
        $authors = null,
        string $description = null
    ) {
        $material = new static();
        $material->type_id = $type_id;
        $material->category_id = $category_id;
        $material->name = $name;
        $material->authors = $authors ?: null;
        $material->description = $description ?: null;
        return $material;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'category_id' => 'Category ID',
            'authors' => 'Authors',
            'description' => 'Description',
        ];
    }
    
    public function updateSelf(
        int $type_id,
        int $category_id,
        string $name, $authors = null,
        string $description = null
    ) {
        $this->type_id = $type_id;
        $this->category_id = $category_id;
        $this->name = $name;
        $this->authors = $authors;
        $this->description = $description;
        return $this;
    }
    
    /**
     * @return Tag[]
     */
    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])->viaTable('tagToMaterial', ['material_id' => 'id'])->all();
    }
    
    /**
     * @return Link[]
     */
    public function getLinks()
    {
        return $this->hasMany(Link::class, ['material_id' => 'id'])->all();
    }
    
    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id'])->one();
    }
    
    /**
     * @return string
     */
    public function getType()
    {
        return MaterialTypes::getLabel($this->type_id);
    }
}
