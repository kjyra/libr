<?php

namespace frontend\models\forms;

use yii\base\Model;

/**
 * Description of TypeForm
 *
 * @author kjyra
 */
class TagForm extends Model
{
    public $name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'min' => '2', 'max' => 255],
            ['name', 'trim'],
        ];
    }
}
