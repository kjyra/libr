<?php

namespace frontend\models\forms;

use yii\base\Model;

/**
 * Description of TypeForm
 *
 * @author kjyra
 */
class LinkForm extends Model
{
    public $name;
    public $url;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['name'], 'string', 'max' => 255],
            ['name', 'trim'],
            ['url', 'url', 'defaultScheme' => 'http']
        ];
    }
}
