<?php

namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\enums\MaterialTypes;

/**
 * Description of TypeForm
 *
 * @author kjyra
 */
class MaterialForm extends Model
{
    public $type;
    public $category;
    public $name;
    public $authors;
    public $desc;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'category', 'name'], 'required'],
            [['name', 'authors'], 'string', 'min' => 2, 'max' => 255],
            [['name', 'authors'], 'trim'],
            [['desc'], 'string', 'min' => 2, 'max' => 1000],
            [['category'], 'integer'],
            ['type', 'in', 'range' => MaterialTypes::getConstantsByName()],
        ];
    }
}
