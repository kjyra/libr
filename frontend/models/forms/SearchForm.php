<?php

namespace frontend\models\forms;

use yii\base\Model;

/**
 * Description of TypeForm
 *
 * @author kjyra
 */
class SearchForm extends Model
{
    public $keyword;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['keyword'], 'string', 'max' => 255],
            ['keyword', 'trim'],
        ];
    }
}
