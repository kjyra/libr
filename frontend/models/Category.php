<?php

namespace frontend\models;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name

 * @property Material[] $materials
 */
class Category extends \yii\db\ActiveRecord
{
    public static function create(string $name)
    {
        $category = new static();
        $category->name = $name;
        return $category;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    
    public function updateSelf(string $name)
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * @return Material[]
     */
    public function getMaterials()
    {
        return Material::find()->where(['category_id' => $this->id])->all();
    }
}
