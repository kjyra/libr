<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use RuntimeException;
use frontend\readModels\MaterialReadRepository;
use frontend\readModels\TagReadRepository;
use frontend\readModels\LinkReadRepository;
use frontend\readModels\CategoryReadRepository;
use frontend\useCases\material\MaterialService;
use frontend\models\forms\MaterialForm;
use frontend\models\forms\TagForm;
use frontend\models\forms\LinkForm;
use frontend\models\forms\SearchForm;
use frontend\models\enums\MaterialTypes;

/**
 * Description of MaterialController
 *
 * @author kjyra
 */
class MaterialController extends Controller
{
    private $materials;
    private $tags;
    private $links;
    private $categories;
    private $handler;
    
    public function __construct(
            $id, 
            $module, 
            MaterialReadRepository $materials, 
            TagReadRepository $tags, 
            LinkReadRepository $links, 
            CategoryReadRepository $categories, 
            MaterialService $handler, 
            $config = []
        )
    {
        parent::__construct($id, $module, $config);
        $this->materials = $materials;
        $this->tags = $tags;
        $this->links = $links;
        $this->categories = $categories;
        $this->handler = $handler;
    }
    
    public function actionIndex()
    {
        $searchForm = new SearchForm();
        
        if(Yii::$app->request->post())
        {
            $searchForm->load(Yii::$app->request->post());      
        }
        
        $materials = $this->materials->all($searchForm->keyword); 
        
        return $this->render('index', [
            'materials' => $materials,
            'searchForm' => $searchForm,
        ]);
    }
    
    public function actionView(int $id)
    {
        $material = $this->materials->get($id);
        $tagForm = new TagForm();
        $tags = $this->tags->findNotInMaterial($material);
            if($tagForm->load(Yii::$app->request->post()) && $tagForm->validate()) {
                try {
                    $this->handler->addTag($tagForm, $material);
                    return $this->redirect(Yii::$app->request->referrer);
                } catch (RuntimeException $ex) {
                    Yii::$app->session->setFlash('error', $ex->getMessage());
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
       
       return $this->render('view', [
           'material' => $material,
           'tags' => $tags,
           'tagForm' => $tagForm,
       ]); 
    }
    
    public function actionCreate()
    {
       $materialForm = new MaterialForm();
       $types = MaterialTypes::listData();
       $categories = $this->categories->all();
       if($materialForm->load(Yii::$app->request->post()) && $materialForm->validate()) {
           try {
               $this->handler->handleCreate($materialForm);
               Yii::$app->session->setFlash('success', 'Материал успешно создан');
               return $this->redirect(['/material']);
           } catch (\Exception $ex) {
               Yii::$app->session->setFlash('error', $ex->getMessage());
           }
       }
       
       return $this->render('create', [
           'materialForm' => $materialForm,
           'types' => $types,
           'categories' => $categories,
       ]); 
    }
    
    public function actionUpdate(int $id)
    {
       $material = $this->materials->get($id);
       $types = MaterialTypes::listData();
       $categories = $this->categories->all();
       $materialForm = new MaterialForm();
       if($materialForm->load(Yii::$app->request->post()) && $materialForm->validate()) {
           try {
               $this->handler->handleUpdate($materialForm, $material->id);
               Yii::$app->session->setFlash('success', 'Материал успешно обновлен');
               return $this->redirect(['/material']);
           } catch (\Exception $ex) {
               Yii::$app->session->setFlash('error', $ex->getMessage());
           }
       }
       
       return $this->render('update', [
           'materialForm' => $materialForm,
           'material' => $material,
           'types' => $types,
           'categories' => $categories,
       ]);
    }
    
    public function actionDelete(int $id)
    {
        $material = $this->materials->get($id);
        try {
            $this->handler->deleteMaterial($material);
            Yii::$app->session->setFlash('success', 'Материал успешно удален.');
        } catch (RuntimeException $ex) {
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionDeleteTag(int $tagId, int $materialId)
    {
        $tag = $this->tags->get($tagId);
        $material = $this->materials->get($materialId);
        try {
            $this->handler->deleteTag($tag, $material);
        } catch (RuntimeException $ex) {
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionDeleteLink(int $linkId)
    {
        $link = $this->links->find($linkId);
        try {
            $this->handler->deleteLink($link);
        } catch (RuntimeException $ex) {
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionLinkModal()
    {
       $material = $this->materials->get(Yii::$app->request->get('id'));
       $linkForm = new LinkForm();
       if($linkForm->load(Yii::$app->request->post())) {
           if($linkForm->validate()) {
                try {
                    $this->handler->handleAddLink($linkForm, $material);
                    return $this->redirect(Yii::$app->request->referrer);
                } catch (RutimeException $ex) {
                    Yii::$app->session->setFlash('error', $ex->getMessage());
                }
           } else {
                Yii::$app->session->setFlash('error', 'Введите ссылку.');
                return $this->redirect(Yii::$app->request->referrer);
           }
       }
       
       return $this->renderPartial('link/link-modal', [
           'linkForm' => $linkForm,
           'material' => $material,
       ]); 
    }
    
    public function actionLinkModalUpdate()
    {
       $link = $this->links->find(Yii::$app->request->get('id'));
       $linkForm = new LinkForm();
       if($linkForm->load(Yii::$app->request->post())) {
           if($linkForm->validate()) {
                try {
                    $this->handler->handleUpdateLink($linkForm, $link->id);
                    return $this->redirect(Yii::$app->request->referrer);
                } catch (\Exception $ex) {
                    Yii::$app->session->setFlash('error', $ex->getMessage());
                }
           } else {
                Yii::$app->session->setFlash('error', 'Введите ссылку.');
                return $this->redirect(Yii::$app->request->referrer);
           }
       }
       return $this->renderPartial('link/link-modal-update', [
           'linkForm' => $linkForm,
           'link' => $link,
       ]);
    }
}
