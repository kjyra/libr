<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\readModels\CategoryReadRepository;
use frontend\useCases\category\CategoryService;
use frontend\models\forms\CategoryForm;

/**
 * Description of TagController
 *
 * @author grigo
 */
class CategoryController extends Controller
{
    private $categories;
    private $handler;

    public function __construct($id, $module, CategoryReadRepository $categories, CategoryService $handler, $config = array())
    {
        $this->categories = $categories;
        $this->handler = $handler;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex() {
        $categories = $this->categories->all();
        return $this->render('index', [
            'categories' => $categories
        ]);
    }

    public function actionCreate()
    {
        $categoryForm = new CategoryForm();
        if ($categoryForm->load(Yii::$app->request->post()) && $categoryForm->validate()) {
            try {
                $this->handler->handleCreate($categoryForm);
                Yii::$app->session->setFlash('success', 'Категория успешно создана.');
                return $this->redirect(['/category']);
            } catch (RuntimeException $ex) {
                Yii::$app->session->setFlash('error', $ex->getMessage());
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('create', [
            'categoryForm' => $categoryForm
        ]);
    }

    public function actionUpdate(int $id)
    {
       $category = $this->categories->get($id);
       $categoryForm = new CategoryForm();
       if($categoryForm->load(Yii::$app->request->post()) && $categoryForm->validate()) {
           try {
               $this->handler->handleUpdate($categoryForm, $category->id);
               Yii::$app->session->setFlash('success', 'Категория успешно обновлена.');
               return $this->redirect(['/category']);
           } catch (\Exception $ex) {
               Yii::$app->session->setFlash('error', $ex->getMessage());
           }
       }
       return $this->render('update', [
           'categoryForm' => $categoryForm,
           'category' => $category,
       ]);
    }
    
    public function actionDelete(int $id)
    {
        $category = $this->categories->get($id);
        try {
            $this->handler->handleDelete($category);
            Yii::$app->session->setFlash('success', 'Категория успешно удалена.');
        } catch (RuntimeException $ex) {
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}
