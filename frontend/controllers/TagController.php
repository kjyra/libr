<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\readModels\TagReadRepository;
use frontend\models\forms\TagForm;
use frontend\useCases\tag\TagService;

/**
 * Description of TagController
 *
 * @author grigo
 */
class TagController extends Controller
{
    private $tags;
    private $handler;

    public function __construct($id, $module, TagReadRepository $tags, TagService $handler, $config = array())
    {
        $this->tags = $tags;
        $this->handler = $handler;
        parent::__construct($id, $module, $config);
    }

    public function actionIndex() {
        $tags = $this->tags->all();
        return $this->render('index', [
            'tags' => $tags
        ]);
    }
    
    public function actionView(int $id) {
        $tag = $this->tags->get($id);
        return $this->render('view', [
            'tag' => $tag
        ]);
    }

    public function actionCreate()
    {
        $tagForm = new TagForm();
        if ($tagForm->load(Yii::$app->request->post()) && $tagForm->validate()) {
            try {
                $this->handler->handleCreate($tagForm);
                Yii::$app->session->setFlash('success', 'Тег успешно создан');
                return $this->redirect(['/tag']);
            } catch (RuntimeException $ex) {
                Yii::$app->session->setFlash('error', $ex->getMessage());
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('create', [
            'tagForm' => $tagForm
        ]);
    }

    public function actionUpdate(int $id)
    {
       $tag = $this->tags->get($id);
       $tagForm = new TagForm();
       if($tagForm->load(Yii::$app->request->post()) && $tagForm->validate()) {
           try {
               $this->handler->handleUpdate($tagForm, $tag->id);
               Yii::$app->session->setFlash('success', 'Тег успешно обновлен');
               return $this->redirect(['/tag']);
           } catch (\Exception $ex) {
               Yii::$app->session->setFlash('error', $ex->getMessage());
           }
       }
       return $this->render('update', [
           'tagForm' => $tagForm,
           'tag' => $tag,
       ]);
    }
    
    public function actionDelete(int $id)
    {
        $tag = $this->tags->get($id);
        try {
            $this->handler->handleDelete($tag);
            Yii::$app->session->setFlash('success', 'Тег успешно удален.');
        } catch (RuntimeException $ex) {
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}
