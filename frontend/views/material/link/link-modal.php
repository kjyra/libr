<?php

/* @var $linkForm frontend\models\forms\LinkForm */

use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(); ?>
<div class="modal-header">
                <h5 class="modal-title" id="exampleModalToggleLabel">Добавить ссылку</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                        <?= $form->field($linkForm, 'name', ['options' => [
                                    'class' => 'form-floating mb-3',
                                ]])->textInput([
                                'name',
                                'class' => 'form-control', 
                                'placeholder' => "Подпись",
                                'id' => 'floatingLinkName'
                            ])->label('Название', [
                                    'for' => 'floatingLinkName',
                                    'class' => false
                            ]); ?>
                
                            <?= $form->field($linkForm, 'url', ['options' => [
                                    'class' => 'form-floating mb-3',
                                ]])->textInput([
                                'url',
                                'class' => 'form-control', 
                                'placeholder' => "Ссылка",
                                'id' => 'floatingLinkUrl'
                            ])->label('Ссылка', [
                                    'for' => 'floatingLinkUrl',
                                    'class' => false
                            ]); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-bs-dismiss="modal">Закрыть</button>
                <?= yii\helpers\Html::submitButton('Добавить', ['class' => 'btn btn-primary']); ?>
            </div>
<?php ActiveForm::end(); ?>