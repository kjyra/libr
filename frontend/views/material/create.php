<?php

/* @var $categories frontend\models\Category[] */
/* @var $types array */
/* @var $materialForm frontend\models\forms\MaterialForm */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<div class="container">
    <h1 class="my-md-5 my-4">Добавить материал</h1>
    <div class="row">
        <div class="col-lg-5 col-md-8">
            <?php $form = ActiveForm::begin(); ?>
            <?=
            $form->field($materialForm, 'type', ['options' => [
                    'class' => 'form-floating mb-3',
            ]])->dropDownList($types, [
                'prompt' => 'Выберите тип',
                'class' => 'form-select input-input',
                'id' => "floatingSelectType",
            ])->label('Тип', [
                'for' => 'floatingSelectType',
                'class' => false
            ]);

            ?>
            <?=
            $form->field($materialForm, 'category', ['options' => [
                    'class' => 'form-floating mb-3',
            ]])->dropDownList(ArrayHelper::map($categories, 'id', 'name'), [
                'prompt' => 'Выберите категорию',
                'class' => 'form-select input-input',
                'id' => "floatingSelectCategory",
            ])->label('Категория', [
                'for' => 'floatingSelectCategory',
                'class' => false
            ]);

            ?>
            <?=
            $form->field($materialForm, 'name', ['options' => [
                    'class' => 'form-floating mb-3',
            ]])->textInput([
                'name',
                'placeholder' => 'Напишите название',
                'id' => 'floatingName'
            ])->label('Название', [
                'for' => 'floatingName',
                'class' => false
            ]);

            ?>
            <?=
            $form->field($materialForm, 'authors', ['options' => [
                    'class' => 'form-floating mb-3',
            ]])->textInput(['authors', 'placeholder' => 'Напишите авторов', 'id' => 'floatingAuthor'])->label('Авторы', [
                'for' => 'floatingAuthor',
                'class' => false
            ]);

            ?>
            <?=
            $form->field($materialForm, 'desc', ['options' => [
                    'class' => 'form-floating mb-3',
            ]])->textarea([
                'placeholder' => 'Напишите краткое описание',
                'id' => 'floatingDescription'
            ])->label('Описание', [
                'for' => 'floatingDescription',
                'class' => false
            ]);

            ?>
<?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']); ?>
<?php ActiveForm::end(); ?>
        </div>
    </div>
</div>