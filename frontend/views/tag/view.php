<?php

/* @var $tag frontend\models\Tag */

use yii\helpers\Url;
?>
<div class="container">
    <h1 class="my-md-5 my-4"><?= $tag->name; ?></h1>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Название</th>
                    <th scope="col">Автор</th>
                    <th scope="col">Тип</th>
                    <th scope="col">Категория</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($tag->getMaterials() as $material): ?>
                    <tr>
                        <td><a href="<?= Url::to(['material/view', 'id' => $material->id]); ?>"><?= $material->name; ?></a></td>
                        <td><?= $material->authors; ?></td>
                        <td><?= $material->getType()->name; ?></td>
                        <td><?= $material->getCategory()->name; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>