<?php

/* @var $tagForm frontend\models\forms\TagForm */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="container">
    <h1 class="my-md-5 my-4">Добавить тег</h1>
    <div class="row">
        <div class="col-lg-5 col-md-8">
<?php $form = ActiveForm::begin(); ?>
            <?=
            $form->field($tagForm, 'name', ['options' => [
                    'class' => 'form-floating mb-3',
            ]])->textInput([
                'name',
                'class' => 'form-control',
                'placeholder' => 'Напишите название',
                'id' => 'floatingName',
            ])->label('Название', [
                'for' => 'floatingName',
                'class' => false
            ])

            ?>
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary validate']); ?>
<?php ActiveForm::end(); ?>
        </div>
    </div>
</div>