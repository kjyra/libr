<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="main-wrapper">
    <div class="content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="<?= UrL::to(['/site']); ?>">Test</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link <?php if(Url::current() === '/material/index'): echo 'active'; endif; ?>" <?php if(Url::current() == '/material/index'): echo 'aria-current="page"'; endif; ?>  href="<?= Url::to(['/material']); ?>">Материалы</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if(Url::current() === '/tag/index'): echo 'active'; endif; ?>" <?php if(Url::current() == '/tag/index'): echo 'aria-current="page"'; endif; ?> href="<?= Url::to(['/tag']); ?>">Теги</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if(Url::current() === '/category/index'): echo 'active'; endif; ?>" <?php if(Url::current() == '/category/index'): echo 'aria-current="page"'; endif; ?> href="<?= Url::to(['/category']); ?>">Категории</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
            <?php if( Yii::$app->session->hasFlash('success') ): ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo Yii::$app->session->getFlash('success'); ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            <?php endif;?>
            <?php if( Yii::$app->session->hasFlash('error') ): ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo Yii::$app->session->getFlash('error'); ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            <?php endif;?>
            <?= $content; ?>
    </div>
    <footer class="footer py-4 mt-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col text-muted">Test</div>
            </div>
        </div>
    </footer>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>